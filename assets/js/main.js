function openLevel(evt, cityName) {
    var i, levelcontent, levellinks;
    levelcontent = document.getElementsByClassName("levelcontent");
    for (i = 0; i < levelcontent.length; i++) {
      levelcontent[i].style.display = "none";
    }
    levellinks = document.getElementsByClassName("levellinks");
    for (i = 0; i < levellinks.length; i++) {
      levellinks[i].className = levellinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();


  var swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 80,
    loop : true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      1024: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      }
    }
  });
